// create function logReceipt that accepts menu items (1 or many) as objects
// with these properties: {descr, price}
// i.e. {descr: 'Coke', price: 1.99}
// function should log each item to the console and log a total price



// Check
logReceipt(
    { descr: 'Burrito', price: 5.99 },
    { descr: 'Chips & Salsa', price: 2.99 },
    { descr: 'Sprite', price: 1.99 }
  );
  // should log something like:
  // Burrito - $5.99
  // Chips & Salsa - $2.99
  // Sprite - $1.99
  // Total - $10.97


const cake = {descr:'Birthday Cake', Qty: '1', price: 12.99};

const candle = {descr:'6 pack candle', Qty: '1', price: 4.99}; 

const cap = {descr:'5 pack Bday-cap', Qty: '1', price: 2.99};

const cookie = {descr:'Chocolate biscuit', Qty: '1', price: 6.99};

const candy = {descr:'Kit Kat',          Qty: '1', price: 4.99};

const logReceipt=(...args)=>{
    let total = 0;
    args.forEach(arg => {

        total += arg.price;
        console.log( `${arg.descr}  -${arg.Qty}-  = $ ${arg.price}`);
    })
    console.log(`Total - ${total.toFixed()}`);
}; 
logReceipt(cake,candle, cap, cookie, candy);